# Sending Message

Sending message app.


## Installation

You need install python 3.9:

```
pip install python
```

## Important import
```
import smtplib
import pywhatkit
import os

```

## Getting started

App strat:

```
python app.py

```
Send message for WhatsApp

```
python app2.py

```
Send message for email.


## Description

App has 2 functions:
```
send_message_inst():
```
Sending message for WhatsApp.
```
def send_message():
```
Sending message WhatsApp with delay.

App2 has 1 function:
```
def send_email(message)
```
Sending message for email.

## Author

This app was done by Dmitry Tsunaev.

- [linkedin](http://linkedin.com/in/dmitry-tsunaev-530006aa)

